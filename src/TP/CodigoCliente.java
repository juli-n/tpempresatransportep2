package TP;

public class CodigoCliente {
	
	public static void main(String[] args) {
//		double volumen;
		Empresa e = new Empresa("30112223334","Expreso Libre" , 4000000);
//		System.out.println(e.toString());
//		System.out.print(e.incorporarPaquete("Buenos Aires", 180, 2, true));
//		e.agregarDestino("Rosario", 100);
//		e.agregarDestino("Buenos Aires", 400);
//		e.agregarDestino("Mar del Plata", 800);
//		e.agregarTrailer("AA333XQ", 10000, 60, true, 2, 100);
//		e.agregarMegaTrailer("AA444PR", 15000, 100, false, 3, 150, 200, 50);
//		e.agregarFlete("AB555MN", 5000, 20, 4, 2, 300);
//		e.asignarDestino("AA333XQ", "Buenos Aires");
//		e.asignarDestino("AB555MN", "Rosario");
//		// paquetes que necesitan frio
//		e.incorporarPaquete("Buenos Aires", 100, 2, true);
//		e.incorporarPaquete("Buenos Aires", 150, 1, true);
//		e.incorporarPaquete("Mar del Plata", 100, 2, true);
//		e.incorporarPaquete("Mar del Plata", 150, 1, true);
//		e.incorporarPaquete("Rosario", 100, 2, true);
//		e.incorporarPaquete("Rosario", 150, 1, true);
//		// paquetes que NO necesitan frio
//		e.incorporarPaquete("Buenos Aires", 200, 3, false);
//		e.incorporarPaquete("Buenos Aires", 400, 4, false);
//		e.incorporarPaquete("Mar del Plata", 200, 3, false);
//		e.incorporarPaquete("Rosario", 80, 2, false);
//		volumen = e.cargarTransporte("AA333XQ");
//		System.out.println("Se cargaron " + volumen +" metros cubicos en el transp AA333XQ");
//	System.out.println(e.iniciarViaje("AA333XQ"));
// 
//		e.agregarTrailer("AC314PI", 12000, 60, true, 5, 100);
//		e.agregarDestino("Cordoba", 100);
//		e.asignarDestino("AC314PI", "Cordoba");
//		e.incorporarPaquete("Cordoba", 100, 5, true);
//		e.incorporarPaquete("Cordoba", 250, 10, true);
//		e.incorporarPaquete("Cordoba", 150, 8, true);
////		emp.incorporarPaquete("Cordoba", 50, 2.5, false); // no es compatible con el transprote
//		e.incorporarPaquete("Cordoba", 300, 15, true);
//		e.incorporarPaquete("Cordoba", 400, 12, true);
////		emp.incorporarPaquete("Cordoba", 125, 5, false); // no es compatible con el transprote
//		volumen = e.cargarTransporte("AC314PI");
//		System.out.println("Se cargaron " + volumen +" metros cubicos en el transp AC314PI");
//;
	
e.agregarDestino("Parana", 30);

e.agregarFlete("AB271NE", 8000, 40, 3, 2, 200);

e.asignarDestino("AB271NE", "Parana");
e.incorporarPaquete("Parana", 100, 5, false);
e.incorporarPaquete("Parana", 400, 12, false);
e.incorporarPaquete("Parana", 50, 8, false);
e.cargarTransporte("AB271NE");

e.agregarFlete("AD235NP", 6000, 30, 2, 1, 100);
e.asignarDestino("AD235NP", "Parana");
e.incorporarPaquete("Parana", 400, 12, false);
e.incorporarPaquete("Parana", 50, 8, false);
e.incorporarPaquete("Parana", 100, 5, false);
e.cargarTransporte("AD235NP");
System.out.println(e.obtenerTransporteIgual("AB271NE"));
	}
	
}

package TP;

public class Paquete {

	private double peso;
	private double volumen;
	private String destino;
	private boolean esFrio;

	public Paquete(double peso, double volumen, String destino, boolean esFrio) {
		this.peso = peso;
		this.volumen = volumen;
		this.destino = destino;
		this.esFrio = esFrio;
	}
	public boolean esFrio() {
		return this.esFrio;
	}
	public double devolverVolumen() {
		return this.volumen;
	}
	
	public double devolverPeso() {
		return this.peso;
	}
	
	public String darDestino() {
		return this.destino;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PAQUETE");
		builder.append(super.toString());
		builder.append("[DESTINO: ");
		builder.append(destino);
		builder.append("]");
		return builder.toString();

	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + (esFrio ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(peso);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(volumen);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paquete other = (Paquete) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (esFrio != other.esFrio)
			return false;
		if (Double.doubleToLongBits(peso) != Double.doubleToLongBits(other.peso))
			return false;
		if (Double.doubleToLongBits(volumen) != Double.doubleToLongBits(other.volumen))
			return false;
		return true;
	}
}
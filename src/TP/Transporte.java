package TP;

import java.util.LinkedList;

public abstract class Transporte {
	protected String ID;
	protected double cargaMax;
	protected double capacidadMax;
	protected boolean equipDeFrio;
	protected double costo;
	protected boolean estaEnViaje;
	protected double volumenCargado;
	protected String tipo;
	protected Paquete p;
	protected LinkedList<Paquete> mercaderia;
	public Transporte() {
		super();
		ID = ID;
		this.cargaMax = cargaMax;
		this.capacidadMax = capacidadMax;
		this.equipDeFrio = equipDeFrio;
		this.costo = costo;
		this.estaEnViaje = estaEnViaje;
		this.volumenCargado = volumenCargado;
		this.tipo = tipo;
		this.p = p;
		this.mercaderia = mercaderia;
		this.destino = destino;
		this.km = km;
	}

	protected String destino;
    private double km;

	
	
	
	//Agrega los paquetes de productos a la lista Mercaderia.
		public abstract void agregarMercaderia(double peso, double volumen, String destino, boolean esFrio);
	
		
	    public boolean cargaigual(LinkedList<Paquete> m) {
	    	boolean esIgual=true;
	    	boolean igual=false;
	    		for(Paquete p2: mercaderia) {
	    			igual=false;
	    			for(Paquete p: m) {
	    			if(p.equals(p2))
	    				igual= igual || true;
	    		}
	    		esIgual= esIgual && igual;
	    	}
			return esIgual;
	    }


	@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((destino == null) ? 0 : destino.hashCode());
			result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Transporte other = (Transporte) obj;
	    	boolean esIgual=true;
	    	boolean igual=false;
	    		for(Paquete p1: this.mercaderia) {
	    			igual=false;
	    			for(Paquete p2: other.mercaderia) {
	    			if(p1.equals(p2))
	    				igual= igual || true;
	    		}
	    			esIgual= esIgual && igual;
	    	}
			
	    
			if (destino == null) {
				if (other.destino != null)
					return false;
			} else if (!destino.equals(other.destino))
				return false;
			if (tipo == null) {
				if (other.tipo != null)
					return false;
			} else if (!tipo.equals(other.tipo))
				return false;
			if (tipo == null) {
				if (other.tipo != null)
					return false;
			} else if (!tipo.equals(other.tipo))
				return false;
//			if(!esIgual)
//				return false;
			return true;
		}


	//Agrega una cadena a destino.
	public  abstract void asignarDestino(String destino);

	public abstract double devolverCosto();

	public double getVolumenOcupado() {
		return volumenCargado;

	}

	public void setestaEnViaje(boolean Enviaje) {
		this.estaEnViaje = Enviaje;

	}

	public boolean getestaEnViaje() {

		return estaEnViaje;

	}

//este metodo vacia la lista Mercaderia.
	public abstract void vaciarCarga();

	public boolean existeDestino() {

		return this.destino!=null;
	}
	public boolean espacioRemolqueDisponible() {
		if(volumenCargado>=capacidadMax) {
			return true;
		}
		return false;
	}

	public abstract boolean remolqueCargado();

	public abstract boolean remolqueVacio();

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public abstract void setVolumenCargado(double volumenTotal);

	public double getCargaMax() {
		// TODO Auto-generated method stub
		return cargaMax;
	}
	
	public abstract boolean equipDeFrio();
	
	public void setKM( double KM) {
		this.km=KM;
	}

	public void setEquipFrio(boolean b) {
		equipDeFrio=b;
		
	}

	public LinkedList<Paquete> getMercaderia() {
		// TODO Auto-generated method stub
		return mercaderia;
	}
}

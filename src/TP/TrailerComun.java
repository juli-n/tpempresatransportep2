package TP;

import java.util.LinkedList;

public class TrailerComun extends Transporte{
	private String ID;
	private double cargaMax;
	private double capacidadMax;
	private boolean equipDeFrio;
	private double costo;
	private boolean estaEnViaje;
	private LinkedList<Paquete> mercaderia;
	private String destino;
	private double seguroDeCarga;
	private double  maxKM;
	private double costoFijo;
	private double km;

	public TrailerComun(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm,
			double segCarga) {
		this.ID = matricula;
		this.cargaMax = cargaMax;
		this.capacidadMax = capacidad;
		this.equipDeFrio = tieneRefrigeracion;
		this.costo = costoKm;
		this.estaEnViaje = estaEnViaje;
		this.mercaderia = new LinkedList<Paquete>();
		this.destino = destino;
		this.seguroDeCarga = segCarga;
		this.maxKM = 500.0;
		this.costoFijo=costoFijo;
		this.volumenCargado=0;
		this.tipo="TrailerComun";
		this.km=km;
	}

	@Override
	public double devolverCosto() {
		// TODO Auto-generated method stub
		return (km*costo)+costoFijo+seguroDeCarga;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TRAILER COMUN");
		builder.append(super.toString());
		builder.append("[MERCADERIA EN REMOLQUE : ");
		builder.append(mercaderia);
		builder.append("]");
		return builder.toString();

	}
	

	@Override
	public void agregarMercaderia(double peso, double volumen, String destino, boolean esFrio) {
		mercaderia.add(new Paquete(peso, volumen, destino, esFrio));
		volumenCargado= volumenCargado+volumen;
		
	}

	@Override
	public void vaciarCarga() {
		this.mercaderia = null;
		volumenCargado=0;
		
	}

	@Override
	public void asignarDestino(String destino) {
		this.destino = destino;
		
	}

	@Override
	public boolean remolqueCargado() {
		return  mercaderia.size()>0 ;
	}
	
    public boolean equipDeFrio() {

		return this.equipDeFrio;
    }

	@Override
	public void setVolumenCargado(double volumenTotal) {
		volumenCargado=volumenTotal;

		
	}
	@Override
	public boolean remolqueVacio() {
		return volumenCargado==0;
	}

	public void setKM( double KM) {
		this.km=KM;
	}
    public boolean cargaigual(LinkedList<Paquete> m) {
	boolean esIgual=true;
	boolean igual=false;
	for(Paquete p: m) {
		for(Paquete p2: mercaderia) {
			if(p.equals(p2))
				igual= igual || true;
		}
		esIgual= esIgual && igual;
	}
	return esIgual;
}
	
}

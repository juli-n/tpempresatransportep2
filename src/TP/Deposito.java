package TP;


import java.util.ArrayList;
import java.util.LinkedList;

public class Deposito {
    private String tipo;
    private double capacidadMax;
	private double volumenOcupado;
	private boolean equipDeRefrigeracion;
	private ArrayList<Paquete> mercaderia;

	public Deposito(String tipo, double capacidadMax, boolean equipDeRefrigeracion) {
		this.tipo = tipo;
		this.capacidadMax = capacidadMax;
		this.equipDeRefrigeracion = equipDeRefrigeracion;
		this.volumenOcupado=volumenOcupado;
		this.equipDeRefrigeracion= equipDeRefrigeracion;
		this.mercaderia= new ArrayList<Paquete>();
	}

	public void agregarMercaderia(double peso, double volumen, String destino, boolean esFrio) {
		mercaderia.add(new Paquete(peso, volumen, destino, esFrio));
		volumenOcupado+=volumen;
	}

	public ArrayList<Paquete> getMercaderia() {
		return mercaderia;
	}

	public double getVolumenOcupado() {
		return volumenOcupado;
	}

	public void setVolumenOcupado(double volumenOcupado) {
		this.volumenOcupado = volumenOcupado;
	}

	public void setMercaderia(ArrayList<Paquete> mercaderia) {
		this.mercaderia = mercaderia;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DEPOSITO");
//		builder.append(super.toString());
		builder.append(tipo);
		builder.append("\n[(Mercaderia\n");
		builder.append(mercaderia);
		builder.append("]");
		return builder.toString();

	}
}

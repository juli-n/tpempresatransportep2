package TP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;
import java.util.LinkedList;

public class Empresa {
	private String nombre;
	private String cuit;
	private int capacidadDeCadaDeposito;
	private HashMap<String, Viaje> viajes;
	private HashMap<String, Transporte> transportes;
	private Deposito depositoComun;
	private Deposito depositoConRefrigeracion;

	// Constructor de la empresa.
	Empresa(String cuit, String nombre, int capacidadDeCadaDeposito) {
		this.cuit = cuit;
		this.nombre = nombre;
		this.capacidadDeCadaDeposito = capacidadDeCadaDeposito;
		this.viajes = new HashMap<String, Viaje>();
		this.transportes = new HashMap<String, Transporte>();
		this.depositoComun = new Deposito("Comun",capacidadDeCadaDeposito, false);
		this.depositoConRefrigeracion = new Deposito("ConRefrigeracion",capacidadDeCadaDeposito, true);
	
	}
	
	

	/* Preguntar si los Strings no se repiten */
	// Incorpora un nuevo destino y su distancia en km.
	// Es requisito previo, para poder asignar un destino a un transporte.
	// Si ya existe el destino se debe generar una excepci�n.
	void agregarDestino(String destino, int km) {
		if (viajes.containsKey(destino)) {
			throw new RuntimeException("No se puede asignar el destino porque ya existe el destino indicado.");
		}
		viajes.put(destino, new Viaje(destino, km));

	}

	// Los siguientes m�todos agregan los tres tipos de transportes a la
	// empresa, cada uno con sus atributos correspondientes.
	// La matr�cula funciona como identificador del transporte.
	void agregarTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm,
			double segCarga) {
		if (!transportes.containsKey(matricula)) {
			transportes.put(matricula,
					new TrailerComun(matricula, cargaMax, capacidad, tieneRefrigeracion, costoKm, segCarga));
			System.out.println(tieneRefrigeracion);
		
		}

	}

	void agregarMegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion,
			double costoKm, double segCarga, double costoFijo, double costoComida) {
		if (!transportes.containsKey(matricula)) {
			transportes.put(matricula, new MegaTrailer(matricula, cargaMax, capacidad, tieneRefrigeracion, costoKm,
					segCarga, costoFijo, costoComida));
		}

	}

	void agregarFlete(String matricula, double cargaMax, double capacidad, double costoKm, int cantAcompaniantes,
			double costoPorAcompaniante) {
		if (!transportes.containsKey(matricula)) {
			transportes.put(matricula, new Flete(matricula, cargaMax, capacidad, costoKm, cantAcompaniantes));
		}
	}

	// Se asigna un destino a un transporte dada su matr�cula (el destino
	// debe haber sido agregado previamente, con el m�todo agregarDestino).
	// Si el destino no est� registrado, se debe generar una excepci�n.
	void asignarDestino(String matricula, String destino) {
		if (transportes.containsKey(matricula) && viajes.containsKey(destino)) {
			transportes.get(matricula).asignarDestino(destino);
			transportes.get(matricula).setKM(viajes.get(destino).getKm());
			System.out.println(viajes);
			
		}if(!viajes.containsKey(destino)) {
			throw new RuntimeException("No est� registrado el destino");

		}

	}

	// Se incorpora un paquete a alg�n dep�sito de la empresa.
	// Devuelve verdadero si se pudo incorporar, es decir,
	// si el dep�sito acorde al paquete tiene suficiente espacio disponible.
	boolean incorporarPaquete(String destino, double peso, double volumen, boolean necesitaRefrigeracion) {
		Paquete p = new Paquete(volumen, peso, destino, necesitaRefrigeracion);
		if (necesitaRefrigeracion && depositoConRefrigeracion.getVolumenOcupado()+volumen <= this.capacidadDeCadaDeposito) {
			depositoConRefrigeracion.agregarMercaderia(peso, volumen, destino, necesitaRefrigeracion);
//			System.out.println(depositoConRefrigeracion);
			return true;

		}
		if (!necesitaRefrigeracion &&  (depositoComun.getVolumenOcupado()+volumen)<=capacidadDeCadaDeposito) {
			depositoComun.agregarMercaderia(peso,volumen , destino, necesitaRefrigeracion);;
			return true;
		}if(capacidadDeCadaDeposito<(depositoConRefrigeracion.getVolumenOcupado()+volumen) || capacidadDeCadaDeposito<(depositoComun.getVolumenOcupado()+volumen) ) {
			return false;
		}
		return false;

	}

	// Dado un ID de un transporte se pide cargarlo con toda la mercader�a
	// posible, de acuerdo al destino del transporte. No se debe permitir
	// la carga si est� en viaje o si no tiene asignado un destino.
	// Utilizar el dep�sito acorde para cargarlo.
	// Devuelve un double con el volumen de los paquetes subidos
	// al transporte.

	
public double cargarTransporte(String matricula) {
		
		double volumenTotal = 0;
		//se podria crear un contador para para cada deposito y evitar errores
		
		//para los depositos refrigerados
		
		if(!transportes.get(matricula).getestaEnViaje()  && transportes.get(matricula).equipDeFrio()) {
			Iterator it = depositoConRefrigeracion.getMercaderia().listIterator();
			while(it.hasNext() && transportes.get(matricula).espacioRemolqueDisponible()) {
				Paquete p = (Paquete) it.next(); 					//necesito castearlo para acceder a los metodos
				transportes.get(matricula).agregarMercaderia(p.devolverPeso(), p.devolverVolumen(), p.darDestino(), p.esFrio());  //agrego la "remolque"
				volumenTotal += p.devolverVolumen(); 				//sumo cada volumen del cada paquete
				it.remove(); 										//remove??? lo tiene que remover
			}
		}
		
		//para los que no necesitan refrigeracion (deposito comun)
		
		if(!transportes.get(matricula).getestaEnViaje()  && !transportes.get(matricula).equipDeFrio()) {
			Iterator itB = depositoComun.getMercaderia().iterator();
			while(itB.hasNext() && transportes.get(matricula).espacioRemolqueDisponible()) {
				Paquete p = (Paquete) itB.next(); //comprobar que se pase el objeto completo! 
				transportes.get(matricula).agregarMercaderia(p.devolverPeso(), p.devolverVolumen(), p.darDestino(), p.esFrio());  //agrego la "remolque"
				volumenTotal += p.devolverVolumen();
				itB.remove();
			}
		}
		
		return volumenTotal;		
	}

	// Inicia el viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// En caso de no tener mercader�a cargada o de ya estar en viaje
	// se genera una excepci�n.
	boolean iniciarViaje(String matricula) {
		if (!transportes.get(matricula).estaEnViaje && !transportes.get(matricula).remolqueVacio()) {
			transportes.get(matricula).setestaEnViaje(true);
              return true;
		}
		if (transportes.get(matricula).estaEnViaje) {
			throw new RuntimeException("El trasporte no puede iniciar el viaje");
		}

		if(transportes.get(matricula).remolqueVacio()) {
			throw new RuntimeException("El trasporte est� vacio");
		}
		return false;

		
			
	}

	// Finaliza el viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// El transporte vac�a su carga y blanquea su destino, para poder
	// ser vuelto a utilizar en otro viaje.
	// Genera excepci�n si no est� actualmente en viaje.
	void finalizarViaje(String matricula) {
		if (transportes.get(matricula).estaEnViaje && !transportes.get(matricula).remolqueVacio()) {
			transportes.get(matricula).setestaEnViaje(false);
			transportes.get(matricula).vaciarCarga();

		} else {
			throw new RuntimeException("El trasporte no est� en viaje");
		}

	}

	// Obtiene el costo de viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// Genera Excepci�n si el transporte no est� en viaje.
	double obtenerCostoViaje(String matricula) {
		Transporte t = transportes.get(matricula);
		Viaje v = viajes.get(t.getDestino());
		if (!t.estaEnViaje) {
			throw new RuntimeException("El trasporte no est� en viaje");

		}
		return t.devolverCosto();

	}

	// Busca si hay alg�n transporte igual en tipo, destino y carga.
	// En caso de que no se encuentre ninguno, se debe devolver null.
	String obtenerTransporteIgual(String matricula) {
		String m= null;
		for (String key : transportes.keySet()) {
			if (transportes.get(matricula).equals(transportes.get(key)) && !(matricula.equals(key))) { 																				// asignado!
				m = key;
			}
		}
		return m;
        
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empresa de Transportes- UNGS \n");
		builder.append("NOMBRE DE EMPRESA: ");
		builder.append(nombre);
		builder.append("\nCuit: ");
		builder.append(cuit);
		builder.append("\nTRANSPORTES REGISTRADOS: \n");
		builder.append(transportes.keySet());
		builder.append("\nDEPOSITO CON REFRIGERACION:");
		builder.append(depositoComun.getMercaderia());
		builder.append("]\n");
		builder.append("\n[DEPOSITO COMUN: \n");
		builder.append(depositoConRefrigeracion.getMercaderia());
		builder.append("]\n");
		builder.append("\nVIAJES REGISTRADOS: \n");
		builder.append(viajes);
		return builder.toString();

	}

}

package TP;

public class Viaje {
	private String destino;
	private double km;
	
	
	
	public Viaje(String destino, double km) {
		this.destino = destino;
		this.km = km;
	}



	public String getDestino() {
		return destino;
	}



	public void setDestino(String destino) {
		this.destino = destino;
	}



	public double getKm() {
		return km;
	}



	public void setKm(double km) {
		this.km = km;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Viaje other = (Viaje) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VIAJE");
//		builder.append(super.toString());
		builder.append("[(DESTINO): ");
		builder.append(destino);
		builder.append("]");
		return builder.toString();

	}



	

}

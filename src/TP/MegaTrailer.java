package TP;

import java.util.LinkedList;

public class MegaTrailer extends  Transporte{
	private String ID;
	private double cargaMax;
	private double capacidadMax;
	private boolean equipDeFrio;
	private double costo;
	private boolean estaEnViaje;
	private LinkedList<Paquete> mercaderia;
	private String destino;
	private double seguroDeCarga;
	private double  maxKM;
	private double costoFijo;
	private double costoPorComida;
	private double volumenCargado;
	private String tipo;
	private double km;
	
	public MegaTrailer(String matricula, double cargaMax2, double capacidad, boolean tieneRefrigeracion, double costoKm,
			double segCarga, double costoFijo, double costoComida) {
		this.ID = matricula;
		this.cargaMax = cargaMax2;
		this.capacidadMax = capacidad;
		this.equipDeFrio = tieneRefrigeracion;
		this.costo = costoKm;
		this.estaEnViaje =false ;
		this.mercaderia = new LinkedList<Paquete>();
		this.destino = destino;
		this.seguroDeCarga = segCarga;
		this.maxKM = 500.0;
		this.costoFijo=costoFijo;
		this.costoPorComida=costoComida;
		this.volumenCargado=0;
		this.tipo="MegaTrailer";
	}

	@Override
	public double devolverCosto() {
		
		return (km*costo)+costoFijo+costoPorComida+seguroDeCarga;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MEGA TRAILER");
		builder.append(super.toString());
		builder.append(ID);
		builder.append("[MERCADERIA EN REMOLQUE: ");
		builder.append(mercaderia);
		builder.append("]");
		return builder.toString();

	}
	

	
    public boolean cargaigual(LinkedList<Paquete> m) {
    	boolean esIgual=true;
    	boolean igual=false;
    	
    		for(Paquete p2: mercaderia) {
    			for(Paquete p: m) {
    			if(p.equals(p2))
    				igual= igual || true;
    		}
    		esIgual= esIgual && igual;
    	}
		return esIgual;
    }
	


	@Override
	public void agregarMercaderia(double peso, double volumen, String destino, boolean esFrio) {
			mercaderia.add(new Paquete(peso, volumen, destino, esFrio));
			volumenCargado= volumenCargado+volumen;

	}

	@Override
	public void vaciarCarga() {
		this.mercaderia = null;
		volumenCargado=0;
		
	}

	@Override
	public boolean remolqueCargado() {
		return  mercaderia.size()>0 ;
	}

	@Override
	public void asignarDestino(String destino) {
		this.destino=destino;
		
	}

    public boolean equipDeFrio() {

		return this.equipDeFrio;
    }
	
	@Override
	public void setVolumenCargado(double volumenTotal) {
		volumenCargado=volumenTotal;

		
	}

	@Override
	public boolean remolqueVacio() {
		return volumenCargado==0;
	}
 
	public void setKM( double KM) {
		this.km=KM;
	}
	public LinkedList<Paquete> getMercaderia() {
		// TODO Auto-generated method stub
		return mercaderia;
	}
}
